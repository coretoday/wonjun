package com.core.today.prism;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView{
	
	public CustomTextView(Context context){
		super(context);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "THEjunggt120.otf"));
	}
	
	public CustomTextView(Context context, AttributeSet attrs){
		super(context, attrs);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "THEjunggt120.otf"));
	}
	
	public CustomTextView(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "THEjunggt120.otf"));
	}

	 @Override
	    public void setPadding(int left, int top, int right, int bottom) {
	        super.setPadding(left, top, right, bottom);
	    }
}

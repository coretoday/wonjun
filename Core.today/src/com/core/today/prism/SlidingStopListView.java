package com.core.today.prism;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class SlidingStopListView extends ListView{

   private boolean mEnableListViewScroll;
   
   
   
   public SlidingStopListView(Context context) {
      super(context);
      Init();
      // TODO Auto-generated constructor stub
   }
   public SlidingStopListView(Context context, AttributeSet attrs){
      super(context,attrs);
      Init();
   }
   public SlidingStopListView(Context context, AttributeSet attrs,int defStyle){
      super(context,attrs,defStyle);
      Init();
   }
   private void Init(){
      mEnableListViewScroll= true;
   }
   
   @Override
   public boolean onTouchEvent(MotionEvent ev){
      if(mEnableListViewScroll){
         return super.onTouchEvent(ev);
      }
      return false;
   }
   @Override
   public boolean onInterceptTouchEvent(MotionEvent ev){
      if(mEnableListViewScroll){
         return super.onInterceptTouchEvent(ev);
    	  //return true;
      }
      return false;
   }
   public void setListViewScrollEnable(){
      mEnableListViewScroll=true;
   }
   public void setListViewScrollDisable(){
      mEnableListViewScroll=false;
   }
   
}
package com.core.today.prism;


public class NewsBrand {
	private String brand;
	private String image;
	private String URL;
	public NewsBrand(String brand, String image, String URL) {
		this.brand = brand;
		this.image = image;
		this.URL = URL;
	}

	public String getbrand(){
		return brand;
	}
	public void setbrand(String brand){
		this.brand = brand;
	}	
	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getURL() {
		return URL;
	}
	public void setURL(String URL) {
		this.URL=URL;
	}

}

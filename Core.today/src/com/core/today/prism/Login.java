/**
 * Copyright 2010-present Facebook.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.core.today.prism;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.core.today.R;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.*;
import com.google.android.gms.common.GooglePlayServicesClient.*;
import com.google.android.gms.plus.PlusClient;


public class Login extends FragmentActivity implements OnClickListener,
ConnectionCallbacks, OnConnectionFailedListener,  PlusClient.OnAccessRevokedListener {

	/////////////////////////////////////////
    private LoginButton login_facebook;
    private TextView greeting;
    private GraphUser user;
    private UiLifecycleHelper uiHelper;
    ////////////////////////////////////////Variables for facebook Login
    
    ///////////////////////////////////////////////////////////////
    private static final String TAG = "Login Activity";
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
	private static final int DIALOG_GET_GOOGLE_PLAY_SERVICES = 1;
	private static final int REQUEST_CODE_SIGN_IN = 1;
	private static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;
	private ProgressDialog mConnectionProgressDialog;
	private PlusClient mPlusClient;
	private ConnectionResult mConnectionResult;	
	private SignInButton mSignInButton;
	private View mSignOutButton;
	private TextView mSignInStatus;
	private View mRevokeAccessButton;
	private String email_google;
    ///////////////////////////////////////////////////////////////Variables for google Login

	 
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            Log.d("HelloFacebook", "Success!");
        }
    };

    @TargetApi(Build.VERSION_CODES.HONEYCOMB) @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        

        setContentView(R.layout.login);
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP  | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////
        login_facebook = (LoginButton) findViewById(R.id.login_facebook);
        /*loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                Login.this.user = user;
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
             
            }
        });*/

        greeting = (TextView) findViewById(R.id.greeting);
        ///////////////////////////////////////////////////////////////login for facebook
        
        
        ///////////////////////////////////////////////////////////////
        mPlusClient = new PlusClient.Builder(this, this, this)
        .setActions(MomentUtil.ACTIONS)
        .setScopes(Scopes.PLUS_LOGIN,"https://www.googleapis.com/auth/userinfo.email")
        .build();
		  
		  mSignInStatus = (TextView) findViewById(R.id.login_google_status);
	        mSignInButton = (SignInButton) findViewById(R.id.login_google);
	        mSignInButton.setOnClickListener(this);
	        mSignOutButton = findViewById(R.id.logout_google);
	        mSignOutButton.setOnClickListener(this);
	        mRevokeAccessButton = findViewById(R.id.revoke_access_button);
	        mRevokeAccessButton.setOnClickListener(this);
	       
		mConnectionProgressDialog = new ProgressDialog(this);
		mConnectionProgressDialog.setMessage("Signing in...");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
         getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //////////////////////////////////////////////////////////////For google Login
    }

    @Override//google
    protected void onStart() {
		Log.v(TAG, "Start");
		
		super.onStart();
		mPlusClient.connect();
    }

    @Override//google
    protected void onStop() {
    	 mPlusClient.disconnect();
    	super.onStop(); 
    }
    
    @Override//google
	public void onClick(View view) {
		 switch(view.getId()) {
         case R.id.login_google:
             int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
             if (available != ConnectionResult.SUCCESS) {
                 showDialog(DIALOG_GET_GOOGLE_PLAY_SERVICES);
                 return;
             }

             try {
                 mSignInStatus.setText("Signing in with Google+");
                 mConnectionResult.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
             } catch (IntentSender.SendIntentException e) {
                 // Fetch a new result to start.
                 mPlusClient.connect();
             }
             break;
         case R.id.logout_google:
             if (mPlusClient.isConnected()) {
                 mPlusClient.clearDefaultAccount();
                 mPlusClient.disconnect();
                 mPlusClient.connect();
             }
             break;
         case R.id.revoke_access_button:
             if (mPlusClient.isConnected()) {
                 mPlusClient.revokeAccessAndDisconnect(this);
                 updateButtons(false /* isSignedIn */);
             }
             break;
     }
	}

	 @Override//google
	    protected Dialog onCreateDialog(int id) {
	        if (id != DIALOG_GET_GOOGLE_PLAY_SERVICES) {
	            return super.onCreateDialog(id);
	        }

	        int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	        if (available == ConnectionResult.SUCCESS) {
	            return null;
	        }
	        if (GooglePlayServicesUtil.isUserRecoverableError(available)) {
	            return GooglePlayServicesUtil.getErrorDialog(
	                    available, this, REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES);
	        }
	        return new AlertDialog.Builder(this)
	                .setMessage("Sign in with Google is not available.")
	                .setCancelable(true)
	                .create();
	    }
	 
    @Override//facebook
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.  Do so in
        // the onResume methods of the primary Activities that an app may be launched into.
        AppEventsLogger.activateApp(this);

        updateUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
        
        ////////////////////////////////////////////////////////////////////////////////
        if (requestCode == REQUEST_CODE_SIGN_IN
                || requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
            if (resultCode == RESULT_OK && !mPlusClient.isConnected()
                    && !mPlusClient.isConnecting()) {
                // This time, connect should succeed.
                mPlusClient.connect();
            }
        }
        //////////////////////////////////////////////////////////////////////google
    }

    @Override//faceboook
    public void onPause() {
        super.onPause();
        uiHelper.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be launched into.
        AppEventsLogger.deactivateApp(this);
    }

    @Override//facebook
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {//facebook
       
        updateUI();
    }
    
    @Override//google
    public void onAccessRevoked(ConnectionResult status) {
        if (status.isSuccess()) {
            mSignInStatus.setText("Revoked access.");
        } else {
            mSignInStatus.setText("Unable to revoke access.");
            mPlusClient.disconnect();
        }
        mPlusClient.connect();
    }
	
	 @Override//google
	    public void onConnected(Bundle connectionHint) {
	        String currentPersonName = mPlusClient.getCurrentPerson() != null
	                ? mPlusClient.getCurrentPerson().getDisplayName()
	                : getString(R.string.unknown_person);
	        mSignInStatus.setText(getString(R.string.signed_in_status, currentPersonName));
	        updateButtons(true);
	        email_google = mPlusClient.getAccountName();
	        Toast.makeText(getBaseContext(), email_google, 1000).show();
	    }

	 @Override//google
	    public void onDisconnected() {
	        mSignInStatus.setText(R.string.loading_status);
	        mPlusClient.connect();
	        updateButtons(false);
	    }

	    @Override//google
	    public void onConnectionFailed(ConnectionResult result) {
	        mConnectionResult = result;
	        updateButtons(false);
	    }
	    
	    private void updateButtons(boolean isSignedIn) {//google
	        if (isSignedIn) {
	            mSignInButton.setVisibility(View.INVISIBLE);
	            mSignOutButton.setEnabled(true);
	            mRevokeAccessButton.setEnabled(true);
	        } else {
	            if (mConnectionResult == null) {
	                // Disable the sign-in button until onConnectionFailed is called with result.
	                mSignInButton.setVisibility(View.INVISIBLE);
	                mSignInStatus.setText(getString(R.string.loading_status));
	            } else {
	                // Enable the sign-in button since a connection result is available.
	                mSignInButton.setVisibility(View.VISIBLE);
	                mSignInStatus.setText(getString(R.string.signed_out_status));
	            }

	            mSignOutButton.setEnabled(false);
	            mRevokeAccessButton.setEnabled(false);
	        }
	    }

    private void updateUI() {//facebook
        Session session = Session.getActiveSession();
        boolean enableButtons = (session != null && session.isOpened());

        if (enableButtons && user != null) {
            
            greeting.setText(getString(R.string.hello_user, user.getFirstName()));
        } else {
           
            greeting.setText(null);
        }
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}

package com.core.today.prism.categoryNews;

import android.graphics.Bitmap;


public class CategoryItemAdapterHelper {
	private String keyword;
	private String image = null;
	private String URL;
	private String Title;
	private Bitmap bmimg;
	public CategoryItemAdapterHelper(String title, String keyword, String image, String URL, Bitmap bmimg) {
		this.keyword = keyword;
		this.image = image;
		this.URL = URL;
		this.Title = title;
		this.bmimg = bmimg;
	}
	public CategoryItemAdapterHelper(String title, String keyword, String image, String URL) {
		this.keyword = keyword;
		this.image = image;
		this.URL = URL;
		this.Title = title;
	}
	public String getKeyword(){
		return keyword;
	}
	public void setKeyword(String keyword){
		this.keyword = keyword;
	}
	public Bitmap getBmimg(){
		return bmimg;
	}
	public void setBmimg(Bitmap bmimg){
		this.bmimg = bmimg;
	}
	
	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		this.Title = title;
	}
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getURL() {
		return URL;
	}
	public void setURL(String URL) {
		this.URL=URL;
	}

}

package com.core.today.prism.categoryNews;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.example.core.today.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("rawtypes")
public class CategoryAdapter extends ArrayAdapter{

	ArrayList<CategoryItemAdapterHelper> jsonfile;
	public int count =0;
	int MaxNewsItems =10;
	String title;
	String ImgURL;
	Context mcontext;
	LayoutInflater inflater;
	View row = null;
	TextView textViewTitle;
	ImageView newsimage;
	int id;
	public CategoryAdapter(Context context, int resource, ArrayList<CategoryItemAdapterHelper> json) {
		super(context, resource);
		mcontext = context;
		id = resource;
		jsonfile = json;
	}

	public int getCount(){
		return jsonfile.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent){	
		
		
		try{
		row = convertView;
		
		inflater = ((Activity) mcontext).getLayoutInflater();
		row = inflater.inflate(R.layout.grid_row, parent, false);                        
		textViewTitle = (TextView) row.findViewById(R.id.title_text);
		newsimage = (ImageView) row.findViewById(R.id.image);
		}
		catch(Exception e){
			Log.e("error4/",e.toString()); 

		}
        try{
           
        	title = jsonfile.get(position).getTitle();
        	ImgURL = jsonfile.get(position).getImage();
        
        }
        catch(Exception e){
        	Log.e("error",e.toString()+" gae"); 
        }          
        if(ImgURL !="null"){
        
        	try{
        		newsimage.setImageBitmap(jsonfile.get(position).getBmimg());	
        		
        	}
        	catch(Exception e){
        		Log.e("error2",e.toString()+ " bitmap ����"); 

        	}        
        }
        try{
        textViewTitle.setText(title);  

        }
        catch(Exception e){
        	Log.e("error3",e.toString()); 
        }
           
		return row;
	}

}





package com.core.today.prism;

import android.graphics.Bitmap;
import android.widget.LinearLayout;




public class TimeLineItem {
	private String title;
	private String url;
	public TimeLineItem(String title, String url) {
		this.title = title;
		this.url = url;
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}	
	public String url(){
		return url;
	}
	
}

package com.core.today.prism.localNews;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.core.today.prism.FullNews;
import com.core.today.prism.localNews.JsonNewsItemForLocal;
import com.example.core.today.R;

import android.content.Context;
import android.content.Intent;
//<<<<<<< HEAD
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
//<<<<<<< HEAD


public final class Fragment_Local extends Fragment implements OnItemClickListener{
    private static final String KEY_CONTENT = "TestFragment:Content";
   private static final int REQUEST_CODE_ANOTHER = 000;
    private static String _content;
    int click_focus = -1; 
    int itemcount=0;
    ViewGroup container;
    View toolbar;
    boolean blclickable = false;
    private Bitmap bmImg;
    Context context = null;
    Animation scaleUpAnimation;
    Animation scaleDownAnimation;
    JsonNewsAdapter adapter;
    private  static int viewcount =0;
    static ListView plv_1 = null;
    static ListView plv_2 = null;
    static ListView plv_3 = null;
    static ListView plv_4 = null;
    String u="u";
    private String localNum;
    ScrollView mScrollview=null;
    
    
    public static Fragment_Local newInstance(String localNum) {
    	Fragment_Local  fragment = new Fragment_Local();
        Bundle args = new Bundle();
        args.putString("localNum", localNum);
        fragment.setArguments(args);
        return fragment;
    }


    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // if ((savedInstanceState != null) ) {
        localNum = getArguments().getString("localNum");
           
  
       // }
        
       
    }
    @Override
    public void onResume(){
    	super.onResume();
    	viewcount =0;

        new AddJsonNewsItemTask().execute();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	this.container = container;
       context = container.getContext(); // get Context
       Log.e("CONTEXT TEST","context id is " + container.getChildCount() );
       //////////////////////////////////////////////////////////////////////////////////////////////////////////////
       StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
  
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       adapter = new JsonNewsAdapter(context, R.layout.listitem_for_localnews , new ArrayList<JsonNewsItemForLocal>());
       LinearLayout layout = new LinearLayout(getActivity());
    ///////////////////////////////////////////////////////////////////////////////add NewsItem to ArrayList
      // adapter = new JsonNewsAdapter(context, R.layout.listitem_for_localnews , m_orders);
       switch(container.getChildCount()){
	       case 0: 
	    	   plv_1 = (ListView) LayoutInflater.from(context).inflate(
	    	             R.layout.layout_listview_in_viewpager, container, false);
		       plv_1.setAdapter(adapter);
		       plv_1.setOnItemClickListener(this);

		       layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		       layout.setGravity(Gravity.CENTER);
		       layout.addView(plv_1);
	       case 1: 
	    	   plv_2 = (ListView) LayoutInflater.from(context).inflate(
	    	             R.layout.layout_listview_in_viewpager, container, false);
		       plv_2.setAdapter(adapter);
		       plv_2.setOnItemClickListener(this);

		       layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		       layout.setGravity(Gravity.CENTER);
		       layout.addView(plv_2);
	       case 2: 
	    	   plv_3 = (ListView) LayoutInflater.from(context).inflate(
	    	             R.layout.layout_listview_in_viewpager, container, false);
		       plv_3.setAdapter(adapter);
		       plv_3.setOnItemClickListener(this);

		       layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		       layout.setGravity(Gravity.CENTER);
		       layout.addView(plv_3);
	       case 3:
	    	   plv_4 = (ListView) LayoutInflater.from(context).inflate(
	    	             R.layout.layout_listview_in_viewpager, container, false);
		       plv_4.setAdapter(adapter);
		       plv_4.setOnItemClickListener(this);

		       layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		       layout.setGravity(Gravity.CENTER);
		       layout.addView(plv_4);
		      
       }
       
       //Log.e("viewcount_Test", "viewcount is  " + viewcount);
    //plv.setOnRefreshListener(TestFragment.this)©; /// set Refresh listener

       
    /////////////////////////////////////////////////////////////////////////////inflate layout and listview
       //Log.e("TEST1", "PROBLEM IS " +plv.getAdapter().getCount());
       return layout;

    }
    
       public class NewsViewHolder{
          TextView title;
       }
    
    /////////////////////////////////////////////////////////////////////////////////////////
       public class JsonNewsAdapter extends ArrayAdapter<JsonNewsItemForLocal>{
          private ArrayList<JsonNewsItemForLocal> items;
    
      public JsonNewsAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItemForLocal> items) {
         super(context, textViewResourceId, items);
         this.items = items;
      }

      
	      @Override
	      public View getView(final int position, View convertView, ViewGroup parent) {    
	         View v = convertView;
	         NewsViewHolder holder = null;
	         if (v == null) {      
	            LayoutInflater vi =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
		       v = vi.inflate(R.layout.listitem_for_localnews, null);
		       holder = new NewsViewHolder();
		       holder.title= (TextView) v.findViewById(R.id.item_title);
		       v.setTag(holder);
	         }
	         else
	         {
	            holder=(NewsViewHolder)v.getTag();
	         }
	        
	         final JsonNewsItemForLocal p = items.get(position);
		     holder.title.setText(p.getTitle());

		     return v;   
	      } 
       }
       class AddJsonNewsItemTask extends AsyncTask<Void, JsonNewsItemForLocal, Void> {
    	    @Override
    	    protected Void doInBackground(Void... unused) {

    	        String line;
    	        String page ="k";
    	        String title = "a"; 
    	        String group = null;
    	        ArrayList<String> corp_name;
    	        ArrayList<String> corp_url;
    	        group = "http://json.core.today/json2/?o=1&opt=1&n=10&lo="+localNum;

    	        JSONObject test = null;
    	        JSONObject corp;
    	        BufferedReader bufreader = null;
    	        HttpURLConnection urlConnection = null;
    	        JSONArray alljson;
    	        URL url1 = null;
    	        ArrayList<JsonNewsItemForLocal> m_orders =  new ArrayList<JsonNewsItemForLocal>();
    	        m_orders.clear();
    	        page = "";
    	        int count =0;
    	        try {

    	           String sample = group;
    	           url1 = new URL(sample);
    	           urlConnection = (HttpURLConnection) url1.openConnection();

    	           bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));

    	           while((line=bufreader.readLine())!=null){
    	              page+=line;
    	           }

    	           alljson = new JSONArray(page);

    	           for(int k =0; k < 10 ; count++){
    	         	  
    	                 test=new JSONObject(alljson.getString(count));
    	                
    	                 JSONArray corpInfo = new JSONArray(test.getString("a"));
    	                 
    	                 	try{
    	                 	  title = test.getString("ti");
    	                 	  corp_name = new ArrayList<String>();
    	                 	  corp_url = new ArrayList<String>();
    	                 	  for(int i = 0; i <corpInfo.length(); i++){
    	 	                	  corp = new JSONObject(corpInfo.getString(i));
    	 	                	  
    	 	                	  corp_name.add(corp.getString("corp"));
    	 	                	  corp_url.add(corp.getString("url"));
    	 	                	//  Log.e("DUNKIN", corp_name.get(i) + "  ");
    	 	                	 
    	                 	  }
    	                 	//  Log.e("TITLE", title);
    	                       m_orders.add(new JsonNewsItemForLocal(title, corp_name, corp_url));
    	                       publishProgress(m_orders.get(k));
    	                       SystemClock.sleep(150);
    	                       Log.e("DUNKIN",m_orders.get(k).getTitle());
    	                       k++;
    	                 	}
    	                     catch(Exception e){
    	                     	Log.e("ERROR","Problem is  "+e);
    	                     }
    	            }
    	           
       	    
    	        }
    	        catch(Exception e){
    	     //Toast.makeText(context, "Fail to parse JSON", 1000).show();
    	        }
    	   
    	        urlConnection.disconnect();
    	       /* for(int i =0; i< m_orders.size();i++)
 	           {
                    publishProgress(m_orders.get(i));

 	           }*/
    	      /////////////////////////////////////////////////////////////////////////////////////////////parse JSON file
    	     	Log.e("VIEWCOUNT_TEST", "viewcount number is  " + viewcount);

	           	 viewcount++;
    	      return(null);
    	    }

    	    @Override
    	    protected void onProgressUpdate(JsonNewsItemForLocal... item) {
    	    	//Log.e("TEST_VIEWGROUP", "The Number of childeren is " +container.getChildCount());
    	    	//plv_1.setAdapter(adapter);
    	    	Log.e("XXXX","CASE IS " + viewcount);
	    	    	if(viewcount==0){
	    	    		Log.e("TESTCASE 0" , "CASE00000000000");
	 	     	       ((ArrayAdapter<JsonNewsItemForLocal>) plv_1.getAdapter()).add(item[0]);
	    	    	}
	    	    	else if(viewcount==1){
	    	    		Log.e("TESTCASE 1" , "CASE11111111");
	 	     	       ((ArrayAdapter<JsonNewsItemForLocal>) plv_2.getAdapter()).add(item[0]);
	    	    	}
	    	    	else if(viewcount==2){
	    	    		Log.e("TESTCASE 2" , "CASE2222222");
	 	     	       ((ArrayAdapter<JsonNewsItemForLocal>) plv_3.getAdapter()).add(item[0]);
	    	    	}
	    	    	else if(viewcount==3){
	    	    		Log.e("TESTCASE 3" , "CASE3333333");
	 	     	       ((ArrayAdapter<JsonNewsItemForLocal>) plv_4.getAdapter()).add(item[0]);
	    	    	}

    	    	}
    	    

    	    @Override
    	    protected void onPostExecute(Void unused) {
    	      Toast
    	        .makeText(context, "DONE for " +viewcount, Toast.LENGTH_SHORT)
    	        .show();
    	    }
    	  }
        ///////////////////////////////////////////////////////////////////////listview adapter

           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position,
	             long id) {
			        ListView listview = (ListView) parent;
			        toolbar= view.findViewById(R.id.toolbar);
			        NewsViewHolder holder = new NewsViewHolder();
			        JsonNewsItemForLocal p = adapter.getItem(position);
			     
			        holder = new NewsViewHolder();
			        holder.title= (TextView) view.findViewById(R.id.item_title);
			    
			        view.setTag(holder);
    
			
           }

         
    
}

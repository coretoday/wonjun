package com.core.today.prism.localNews;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.today.prism.categoryNews.CategoryItem;
import com.example.core.today.R;

public class MapActivity extends Activity{
	
	 View kk_img;
	 View kw_img;
	 View cc_img;
	 View jr_img;
	 View ks1_img;
	 View ks2_img;
	 View jj_img;
	 ImageView kk;
	 ImageView kw;
	 ImageView cc;
	 ImageView jr;
	 ImageView ks;
	 ImageView jj;
	 @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.map);
         
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_SHOW_TITLE |actionBar.DISPLAY_USE_LOGO);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("Prism");
		
		////////////////////////////////////////////////////////////////////////////////
         kk=(ImageView)findViewById(R.id.kg);
         kw=(ImageView)findViewById(R.id.gw);
         cc=(ImageView)findViewById(R.id.cc);
         jr=(ImageView)findViewById(R.id.jr);
         ks=(ImageView)findViewById(R.id.ks);
         jj=(ImageView)findViewById(R.id.jj);
         /////////////////////////////////////////////////////////
    	 kk_img = (View)findViewById(R.id.btn_kk);
    	 kw_img = (View)findViewById(R.id.btn_kw);
    	 cc_img = (View)findViewById(R.id.btn_cc);
    	 jr_img = (View)findViewById(R.id.btn_jr);
    	 ks1_img = (View)findViewById(R.id.btn_ks1);
    	 ks2_img = (View)findViewById(R.id.btn_ks2);
    	 jj_img = (View)findViewById(R.id.btn_jj);
    	 //////////////////////////////////////////////////////////
    	 kk_img.setOnTouchListener(mTouchEvent);
    	 kw_img.setOnTouchListener(mTouchEvent);
    	 cc_img.setOnTouchListener(mTouchEvent);
    	 jr_img.setOnTouchListener(mTouchEvent);
    	 ks1_img.setOnTouchListener(mTouchEvent);
    	 ks2_img.setOnTouchListener(mTouchEvent);
    	 jj_img.setOnTouchListener(mTouchEvent);
    	 //////////////////////////////////////////////////////////

	 }
	 private OnTouchListener mTouchEvent = new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				int id = v.getId();
				if(action == MotionEvent.ACTION_DOWN){
					switch (id) {
					case R.id.btn_kk:
						kk.setImageResource(R.drawable.kg_w);
						break;
						
					case R.id.btn_kw:
						kw.setImageResource(R.drawable.gw_w);
						break;
						
					case R.id.btn_cc:
						cc.setImageResource(R.drawable.cc_w);
						break;
						
					case R.id.btn_jr:
						jr.setImageResource(R.drawable.jr_w);
						break;
						
					case R.id.btn_ks1:
						ks.setImageResource(R.drawable.ks_w);
						break;
						
					case R.id.btn_ks2:
						ks.setImageResource(R.drawable.ks_w);
						break;	
						
					case R.id.btn_jj:
						jj.setImageResource(R.drawable.jj_w);
						break;	
					default:
						break;
					}
				}
				else if(action == MotionEvent.ACTION_UP){
					switch (id) {
					case R.id.btn_kk:
						kk.setImageResource(R.drawable.kg);
						break;
						
					case R.id.btn_kw:
						kw.setImageResource(R.drawable.gw);
						break;
						
					case R.id.btn_cc:
						cc.setImageResource(R.drawable.cc);
						break;
						
					case R.id.btn_jr:
						jr.setImageResource(R.drawable.jr);
						break;
						
					case R.id.btn_ks1:
						ks.setImageResource(R.drawable.ks);
						break;
						
					case R.id.btn_ks2:
						ks.setImageResource(R.drawable.ks);
						break;	
						
					case R.id.btn_jj:
						jj.setImageResource(R.drawable.jj);
						break;	
					default:
						break;
					}
					Intent intent = new Intent(getBaseContext(), LocalNews.class);
				    intent.putExtra("LocalInfo", id);
				    startActivity(intent);
				}
				return true;
			}
		};
		
		 @Override
	     	public boolean onOptionsItemSelected(MenuItem item) {
	     		if(android.R.id.home == item.getItemId()){
	     			finish();
	     		}
	     		return super.onOptionsItemSelected(item);
	 	}
	 
}

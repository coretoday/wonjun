package com.core.today.prism.localNews;

import java.util.ArrayList;

import viewpagerindicator.TabPageIndicator;

import com.core.today.prism.TestFragment;
import com.example.core.today.R;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class LocalNews extends FragmentActivity{
	private ViewPager mViewPager;
	private static final String[] CONTENT = new String[] { "시사", "스포츠", "연예" };
	private int get_id;
	private ArrayList<String> localInfo = new ArrayList<String>();
	 @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.local_news);	
         
			////////////////////////////////////////////////////////////////////////////////
	        ActionBar actionBar = getActionBar();
	 		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
	 		getActionBar().setHomeButtonEnabled(true);
	 		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
	 	    TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
	 	    getActionBar().setTitle("Prism");
	 		getActionBar().setDisplayOptions(actionBar.DISPLAY_SHOW_TITLE |actionBar.DISPLAY_USE_LOGO);
	 	    Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
	 	    actionBarTitleView.setTypeface(font1);
	 	    getActionBar().setTitle("Prism");
	 	    
			////////////////////////////////////////////////////////////////////////////////

	 	    Intent receivedintent = getIntent();
			get_id = receivedintent.getExtras().getInt("LocalInfo");
			if(get_id == R.id.btn_kk)
			{	
				localInfo.add("서울");
				localInfo.add("경기");
				localInfo.add("인천");
			}
			else if(get_id == R.id.btn_kw)
			{	
				localInfo.add("강원도");
			}
			else if(get_id == R.id.btn_cc)
			{	
				localInfo.add("충청도");
				localInfo.add("대전");
				localInfo.add("세종");
			}
			else if(get_id == R.id.btn_jr)
			{	
				localInfo.add("전라도");
				localInfo.add("광주");
			}
			else if(get_id == R.id.btn_ks1)
			{	
				localInfo.add("경상도");
				localInfo.add("대구");
				localInfo.add("울산");
				localInfo.add("부산");
			}
			else if(get_id == R.id.btn_ks2)
			{	
				localInfo.add("경상도");
				localInfo.add("대구");
				localInfo.add("울산");
				localInfo.add("부산");
			}
			else if(get_id == R.id.btn_jj)
			{	
				localInfo.add("제주도");
			}
			////////////////////////////////////////////////////////////////////////////////
			FragmentPagerAdapter adapter = new ListViewPagerAdapter(getSupportFragmentManager());
			mViewPager = (ViewPager) findViewById(R.id.vp_list);
			mViewPager.setAdapter(adapter);
			mViewPager.setOffscreenPageLimit(3);
			TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.indicator);
			indicator.setViewPager(mViewPager);
			////////////////////////////////////////////////////////////////////////////////activate fragment
	 }
	 
	 private class ListViewPagerAdapter extends FragmentPagerAdapter {

			public ListViewPagerAdapter(FragmentManager fm) {
				super(fm);
				// TODO Auto-generated constructor stub
			}
			
		    @Override
		    public Fragment getItem(int position) {
		    	if(get_id == R.id.btn_kk)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("1");
		             case 1:
		                 return Fragment_Local.newInstance("10");
		             case 2:
		                 return Fragment_Local.newInstance("6");
		             default:
		            	 return null;
					}
				}
				else if(get_id == R.id.btn_kw)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("9");
		             default:
		            	 return null;
					}
				}
				else if(get_id == R.id.btn_cc)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("11");
		             case 1:
		                 return Fragment_Local.newInstance("4");
		             case 2:
		                 return Fragment_Local.newInstance("8");
		             default:
		            	 return null;
					}
				}
				else if(get_id == R.id.btn_jr)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("13");
		             case 1:
		                 return Fragment_Local.newInstance("5");
		             default:
		            	 return null;
					}
				}
				else if(get_id == R.id.btn_ks1)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("12");
		             case 1:
		                 return Fragment_Local.newInstance("3");
		             case 2:
		                 return Fragment_Local.newInstance("7");
		             case 3:
		            	 return Fragment_Local.newInstance("2");
		             default:
		            	 return null;
					}
				}
				else if(get_id == R.id.btn_ks2)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("12");
		             case 1:
		                 return Fragment_Local.newInstance("3");
		             case 2:
		                 return Fragment_Local.newInstance("7");
		             case 3:
		            	 return Fragment_Local.newInstance("2");
		             default:
		            	 return null;
					}
				}
				else if(get_id == R.id.btn_jj)
				{	
					switch(position){
		        	 case 0: 
		        		 return Fragment_Local.newInstance("15");
		             default:
		            	 return null;
					}
				}
				else
					return null;
		    }

		    @Override
		    public CharSequence getPageTitle(int position) {
		        return localInfo.get(position);
		    }

		    @Override
		    public int getCount() {
		      return localInfo.size();
		    }
	 }
	  @Override
     	public boolean onOptionsItemSelected(MenuItem item) {
     		if(android.R.id.home == item.getItemId()){
     			finish();
     		}
     		return super.onOptionsItemSelected(item);
 	}
}


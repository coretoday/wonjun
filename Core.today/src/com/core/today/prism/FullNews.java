package com.core.today.prism;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.core.today.R;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class FullNews extends Activity{
	private Context mcontext;
	private GridView                mGridView;
	private FullNewsAdapter    mAdapterText;
	private ArrayList<String> ht1 = new ArrayList<String>();
	private ArrayList<String> ht2 = new ArrayList<String>();
//	private ArrayList<String> ht3 = new ArrayList<String>();
//	private ArrayList<String> ht4 = new ArrayList<String>();
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_news);
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_TITLE);
		mcontext = this;
		////////////////////////////////////////////////////////////////////////////////
		Intent getintent = getIntent();
		String news_url = getintent.getExtras().getString("news_url");
		int position = getintent.getExtras().getInt("position");
//		Hashtable<String, String> ht_title = new Hashtable<String, String>();
//		Hashtable<String, String> ht_url = new Hashtable<String, String>();

		news_url += "&opt=2";
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//Parsing//				
		URL url1;
		BufferedReader bufreader = null;
	    HttpURLConnection urlConnection = null;
	    String line = "";
	    String page = "";
	    JSONArray news_array = null;
	    JSONObject news_object = null;
	    JSONObject cursor = null;
	    String hi  = "";
	    try
	    {
	    	url1 = new URL(news_url);
		
	    	urlConnection = (HttpURLConnection) url1.openConnection();

	    	bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));

	    	while((line=bufreader.readLine())!=null)
	    	{
	    		page+=line;
	    	}

	    }
	    catch(Exception e){
	    	Log.e("url","웹부터 에러다 벙충아");
	    
	    }
	    
	    
	    try{
	    	news_array = new JSONArray(page);
	    }
	    catch(Exception e){
	    	Log.e("첫 json", "에러다 + " + e.toString());
	    	
	    }
	    try{
	    	
	    	news_object = new JSONObject(news_array.getString(position));
	    	
	    }
	    catch(Exception e ){
	    	Log.e("두번�� json", "에러다 " + e.toString());
	    
	    }
	    try{	
	    	news_array = new JSONArray(news_object.getString("a"));
	    	
	    }
	    catch(Exception e){
	    	Log.e("세번�� json", "에러여 " + e.toString());
	    }
	//    	Toast.makeText(getApplication(), news_array.getString(0), 100000).show();
	    try{
	    	for(int i =0; i < news_array.length(); i++){
	    		cursor = new JSONObject(news_array.getString(i));	
	    		hi += cursor.getString("corp");
//	    		ht_title.put(cursor.getString("corp"), cursor.getString("corp"));
//	    		ht_url.put(cursor.getString("corp"), cursor.getString("url"));
	    		if(!ht1.contains(cursor.getString("corp"))|| ht1.isEmpty()){
	    		ht1.add(cursor.getString("corp"));
	    		ht2.add(cursor.getString("url"));
	    		}
	    	}
	    	Log.e("taggggg", hi);
	    	Log.e("sizesizesize", String.valueOf(ht1.size()));
	    }
	   
	    catch(Exception e)
	    {
	    	Log.e("tag", e.toString() + " 여기서에러");
	 //   	finish();
	    }
//	    Toast.makeText(getApplication(), hi, 100000).show();
	    //End//
	    
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    
	    //Adapter 적용//
        mAdapterText = new FullNewsAdapter(mcontext, R.layout.full_news_item, ht1, ht2);
         
      
        mGridView = (GridView) findViewById(R.id.full_gridView1);
        Log.e("어댑터 연결 전~", "하이") ;
        
        try{
       
        mGridView.setAdapter(mAdapterText);
        }
        catch(Exception e){
        	Log.e("어대터연결", "에러!@!!@!@!@   " + e.toString());
        }
      
        mGridView.setOnItemClickListener(onClickListItem);
        
        //End//
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    }  
    private OnItemClickListener onClickListItem = new OnItemClickListener() {  	 
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {    
        	Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ht2.get(arg2)));
        	startActivity(webIntent);
        }		
    };
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
    public class FullNewsAdapter extends ArrayAdapter{
    	LayoutInflater inflater;
    	View row;
    	TextView brand_title;
    	ArrayList<String> brand_name, brand_url;
    	
    	public FullNewsAdapter(Context context, int resource, ArrayList<String> ht1, ArrayList<String> ht2){
    		super(context,resource);
    		brand_name = ht1;
    		brand_url = ht2;
    		Log.e("생성자 어댑터", "여긴정상?!");
    	}
    	public int getCount(){
    		return ht1.size();
    	}
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		Log.e("갯 뷰", "에러를 찾자");
            final Context context = parent.getContext();
            row = convertView;
            Log.e("왜에러냐 ", " 제길");
    		try{
    			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			row = inflater.inflate(R.layout.full_news_item, parent, false);
    		}
    		catch(Exception e){
    			Log.e("뷰뿌리는데서 에러!", e.toString() + "뷰 뿌리는데서 에러다...");
    		}
    				
    		try{
    			brand_title = (TextView) row.findViewById(R.id.full_news_brand);
    			if(position < getCount())
    			brand_title.setText(brand_name.get(position));
    		}
    		
    		catch(Exception e){
    			Log.e("텍스트 뷰 에러!", e.toString() + "어레이 리스트에서 에러다");
    		}
    		return row;
    	}
    }
}

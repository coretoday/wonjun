package com.core.today.prism;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView3 extends TextView{
	
	public CustomTextView3(Context context){
		super(context);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "Pacifico.ttf"));
	}
	
	public CustomTextView3(Context context, AttributeSet attrs){
		super(context, attrs);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "Pacifico.ttf"));
	}
	
	public CustomTextView3(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "Pacifico.ttf"));
	}
}
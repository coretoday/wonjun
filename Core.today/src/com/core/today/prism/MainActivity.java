package com.core.today.prism;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import viewpagerindicator.TabPageIndicator;
import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.internal.view.menu.MenuView.ItemView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.core.today.prism.categoryNews.CategoryItem;
import com.core.today.prism.localNews.MapActivity;
import com.core.today.prism.searchNews.SearchNews;
import com.example.core.today.R;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshListView;


public class MainActivity extends FragmentActivity implements OnClickListener, OnQueryTextListener  {
	private static final String[] CONTENT = new String[] { "시사", "스포츠", "연예" };
	boolean isPageOpen = false;
	Animation translateLeftAnim;
	Animation translateRightAnim;
	LinearLayout leftmenu;
	LinearLayout frontpage;
	LinearLayout empty;
	FrameLayout main;
	//ImageView menu_btn;
	Button login_btn;
	View sports;
	View politics;
	View economy;
	View it;
	View ranking;
	View entertain;
	View global;
	View timeline_btn;
	View map_btn;
	static final int LOGIN = 0;
	static final int PROFILE = 1;
	static final int MENU_SET_MODE = 2;
	static final int MENU_DEMO = 3;
	private PullToRefreshListView mPullRefreshListView;
	private final long FINISH_INTERVAL_TIME = 2000;  
	private long backPressedTime = 0;
	private final boolean IS_ICON_ITEM = true;
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_ptr_list_in_vp);	
		
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
	    TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
	    getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_SHOW_TITLE |actionBar.DISPLAY_USE_LOGO);
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
	    actionBarTitleView.setTypeface(font1);
	    getActionBar().setTitle("Prism");
		////////////////////////////////////////////////////////////////////////////////
		FragmentPagerAdapter adapter = new ListViewPagerAdapter(getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.vp_list);
		mViewPager.setAdapter(adapter);
		mViewPager.setOffscreenPageLimit(2);
		TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.indicator);
	    indicator.setViewPager(mViewPager);
	    ////////////////////////////////////////////////////////////////////////////////activate fragment
	   
	    //menu_btn = (ImageView)findViewById(R.id.menu_btn);  
	    empty=(LinearLayout) findViewById(R.id.empty);
	    frontpage=(LinearLayout)findViewById(R.id.abc);
	    leftmenu=(LinearLayout)findViewById(R.id.leftmenu);
	    login_btn= (Button)findViewById(R.id.login_total);
	    
	    
	    leftmenu.setOnClickListener(this);
	    frontpage.setOnClickListener(this);
	    empty.setOnClickListener(this);  
	    //menu_btn.setOnClickListener(this);
	    login_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent login_intent = null;
				login_intent = new Intent(MainActivity.this, Login.class);
				startActivity(login_intent);
				
			}
		
		});
	    
	    /////////////////////////////////////////////////////////////////////////////////////
	    translateLeftAnim = AnimationUtils.loadAnimation(this, R.anim.translate_left);
	    translateRightAnim = AnimationUtils.loadAnimation(this, R.anim.translate_right);
	    SlidingPageAnimationListener animListener = new SlidingPageAnimationListener();
	    translateLeftAnim.setAnimationListener(animListener);
	    translateRightAnim.setAnimationListener(animListener);
	    ////////////////////////////////////////////////////////////////////////////////leftmenu Animation
	    
	   
//	    TextView timeline_btn = (TextView)findViewById(R.id.timeline);
	    sports   =(View)findViewById(R.id.ctg_sports);
	    politics =(View)findViewById(R.id.ctg_politics);
	    ranking  =(View)findViewById(R.id.ctg_main);
	    entertain=(View)findViewById(R.id.ctg_entertain);
	    it=(View)findViewById(R.id.ctg_it);
	    economy  =(View)findViewById(R.id.ctg_economy);
	    global = (View)findViewById(R.id.ctg_global);
	    timeline_btn= (View)findViewById(R.id.timeline);
	    map_btn = (View)findViewById(R.id.map);
	    
	    ////////////////////////////////////////////////////////////////////////////////////
	    ranking.setOnTouchListener(mTouchEvent);
	    sports.setOnTouchListener(mTouchEvent);
	    politics.setOnTouchListener(mTouchEvent);
	    entertain.setOnTouchListener(mTouchEvent);
	    it.setOnTouchListener(mTouchEvent);
	    economy.setOnTouchListener(mTouchEvent);
	    global.setOnTouchListener(mTouchEvent);
	    timeline_btn.setOnTouchListener(mTouchEvent_Timeline);
	    map_btn.setOnTouchListener(mTouchEvent_Map);
	    ////////////////////////////////////////////////////////////////////////////////////
	    map_btn.setOnClickListener(new OnClickListener(){
	    	@Override
		    public void onClick(View v) {
			    Intent intent = new Intent(getBaseContext(), MapActivity.class);
			    startActivity(intent);
	    	}
	    });

	}
	
	private OnTouchListener mTouchEvent = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();
			int id = v.getId();
			if(action == MotionEvent.ACTION_DOWN){
				v.setBackgroundColor(Color.parseColor("#242424"));
			}
			else if(action == MotionEvent.ACTION_UP){
				v.setBackgroundColor(Color.parseColor("#382E3C"));
				Intent intent = new Intent(getBaseContext(), CategoryItem.class);
			    intent.putExtra("category", id);
			    startActivity(intent);
			}
			return true;
		}
	};
	private OnTouchListener mTouchEvent_Timeline = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();
			int id = v.getId();
			if(action == MotionEvent.ACTION_DOWN){
				v.setBackgroundColor(Color.parseColor("#242424"));
			}
			else if(action == MotionEvent.ACTION_UP){
				v.setBackgroundColor(Color.parseColor("#382E3C"));
				Intent intent = new Intent(getBaseContext(), TimeLine.class);
			    startActivity(intent);
			}
			return true;
		}
	};
	
	private OnTouchListener mTouchEvent_Map = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();
			int id = v.getId();
			if(action == MotionEvent.ACTION_DOWN){
				v.setBackgroundColor(Color.parseColor("#242424"));
			}
			else if(action == MotionEvent.ACTION_UP){
				v.setBackgroundColor(Color.parseColor("#382E3C"));
				Intent intent = new Intent(getBaseContext(), MapActivity.class);
			    startActivity(intent);
			}
			return true;
		}
	};
    private class SlidingPageAnimationListener implements AnimationListener {
    	/**
    	 * 좌측 메뉴 애니메이션이 끝날 때 호출되는 메소드
    	 */
		public void onAnimationEnd(Animation animation) {
			if(isPageOpen)
			{
				
				frontpage.setClickable(true);
			}
			else
			{
				
				frontpage.setClickable(false);
			}
		}

		public void onAnimationRepeat(Animation animation) {
			

		}

		public void onAnimationStart(Animation animation) {
			if(isPageOpen)
			{
				leftmenu.setVisibility(View.INVISIBLE);
				empty.setVisibility(View.INVISIBLE);
				isPageOpen=false;
			}
			else
			{
				isPageOpen=true;
				empty.setVisibility(View.VISIBLE);
			}
		}

    }

    private class ListViewPagerAdapter extends FragmentPagerAdapter {

		public ListViewPagerAdapter(FragmentManager fm) {
			super(fm);
			// TODO Auto-generated constructor stub
		}
		
        @Override
        public Fragment getItem(int position) {

        	switch(position){
	        	 case 0: 
	        		 return TestFragment.newInstance("정치사회");
	             case 1:
	                 return TestFragment.newInstance("스포츠");
	             case 2:
	                 return TestFragment.newInstance("연예");
	             default:
	            	 return null;
        	}
        	
        	
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }



	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem searchMenu = menu.findItem(R.id.menu_search);
		
		
		SearchView searchView = (SearchView)searchMenu.getActionView();
		
		searchView.setOnQueryTextListener(this);
		searchView.setSubmitButtonEnabled(true);
		
		searchView.setQueryHint("키워드를 입력해보세요");
		
		if(IS_ICON_ITEM){
			searchMenu.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		}else{
			searchMenu.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			searchView.setIconifiedByDefault(false);
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText){
		return false;
	}
	
	@Override
	public boolean onQueryTextSubmit(String query){//검색 돋보기 버튼 눌렀을 때 intent

		Intent intent = new Intent(this, SearchNews.class);
		intent.putExtra("query_value",query);
		startActivity(intent);
		//Toast.makeText(getApplicationContext(), query, 1000).show();

		return false;
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;

		menu.add("Item 1");
		menu.add("Item 2");
		menu.add("Item 3");
		menu.add("Item 4");

		super.onCreateContextMenu(menu, v, menuInfo);
	}


	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			//Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
			break;
		
		case android.R.id.home:
			
		if(isPageOpen == false){
			leftmenu.setVisibility(View.VISIBLE);
			leftmenu.startAnimation(translateRightAnim);
		}			
		else if(isPageOpen){
			leftmenu.startAnimation(translateLeftAnim);
		}
			
		break;
		}
		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
		////////////////////////////////////////////////////////////
		if(v.getId()==android.R.id.home)
		{
			leftmenu.setVisibility(View.VISIBLE);
			leftmenu.startAnimation(translateRightAnim);
		}
		else if(isPageOpen)
		{
			if(v.getId()!=R.id.leftmenu)
			{
				leftmenu.startAnimation(translateLeftAnim);
			}
		}
		///////////////////////////////////////////////////////////leftmenu animation
		
		
	}
	
	@Override 
     public void onBackPressed() {
		long tempTime        = System.currentTimeMillis();
	    long intervalTime    = tempTime - backPressedTime;
	  
	   			
		if(isPageOpen){
			leftmenu.startAnimation(translateLeftAnim);
		}
		else{
	        if ( 0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime ) 
	        {
	        	super.onBackPressed(); 
	        } 
	        else 
	        { 
	         	backPressedTime = tempTime; 
	         	Toast.makeText(getApplicationContext(),"'뒤로' 버튼을 한 번 더 누르시면 종료됩니다.",Toast.LENGTH_SHORT).show(); 
	        } 
		}
      } 



	
}

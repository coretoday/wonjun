package com.core.today.prism.searchNews;

import java.util.ArrayList;
import java.util.Collections;

import android.graphics.Bitmap;


public class JsonNewsItemForSearch {
	private String title;
	private ArrayList<String> corp_name = new ArrayList<String>();
	private ArrayList<String> corp_url  = new ArrayList<String>();
	private String image_url;
	private Bitmap bmImg;
	private int date;
	
//<<<<<<< HEAD
	
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
	
	/**
	 * Initialize with icon and data array
	 */
	

	/**
	 * Initialize with icon and strings
	 */
	public JsonNewsItemForSearch(String title, ArrayList<String> corp_name, ArrayList<String> corp_url,
								 String image_url, Bitmap bmImg, int date) {
		this.title = title;
		for(int i =0; i<corp_name.size();i++)
		{
			this.corp_name.add(corp_name.get(i));
			this.corp_url.add(corp_url.get(i));
		}
		this.image_url = image_url;
		this.bmImg = bmImg;
		this.date = date;
		
	}
	

	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<String> getCorpName() {
		return corp_name;
	}

	public void setCorpName(ArrayList<String> corp_name) {
		for(int i =0; i<corp_name.size();i++)
		{
			this.corp_name.add(corp_name.get(i));
		}
	}
	
	public ArrayList<String> getCorpUrl() {
		return corp_url;
	}

	public void setCorp_Url(ArrayList<String> corp_url) {
		for(int i =0; i<corp_name.size();i++)
		{
			this.corp_url.add(corp_url.get(i));
		}
	}
	
	public String getImageUrl() {
		return image_url;
	}

	public void setImageUrl(String image_url) {
		this.image_url = image_url;
	}
	
	public Bitmap getbmImg() {
		return bmImg;
	}

	public void setbmImg(Bitmap bmImg) {
		this.bmImg=bmImg;
	}
	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}
	/**
	 * Compare with the input object
	 * 
	 * @param other
	 * @return
	 */
	public boolean compareTo(JsonNewsItemForSearch other) {
		if (title.equals(other.getTitle())) {
			return false;
		} else if (corp_url.equals(other.getCorpUrl())) {
			return false;
		} 
		
		return true;
	}

}

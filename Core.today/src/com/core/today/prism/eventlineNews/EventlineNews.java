package com.core.today.prism.eventlineNews;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.core.today.prism.localNews.JsonNewsItemForLocal;
import com.core.today.prism.searchNews.SearchNewsAdapter;
import com.example.core.today.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class EventlineNews extends Activity implements
										AdapterView.OnItemClickListener, StickyListHeadersListView.OnHeaderClickListener,
										StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
										StickyListHeadersListView.OnStickyHeaderChangedListener {
	
	
	private ArrayList<JsonNewsItemForEvent> pager = new ArrayList<JsonNewsItemForEvent>();
	private StickyListHeadersListView stickyList;
	private EventlineNewsAdapter mAdapter;
	private String query;
	
    private boolean fadeHeader = true;

 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_news);
		
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
		
		Intent intent = getIntent();
		query = intent.getExtras().getString("query_value");
		
		pager.addAll(SearchQueryMaker(query));
		
		if(pager.size()!=0) {
			mAdapter = new EventlineNewsAdapter(this, 0, pager);
		}
		else{
			this.finish();
			Toast.makeText(getApplicationContext(), query +"에 대한 결과값이 존재하지 않습니다", 3000).show();
		}
		
		TextView searchTitle = (TextView)findViewById(R.id.search_title);
		searchTitle.setText("검색어 : " + query);
		
		stickyList = (StickyListHeadersListView) findViewById(R.id.search_listview);

        stickyList.setOnItemClickListener(this);
        stickyList.setOnHeaderClickListener(this);
        stickyList.setOnStickyHeaderChangedListener(this);
        stickyList.setOnStickyHeaderOffsetChangedListener(this);
        
       // stickyList.addHeaderView(getLayoutInflater().inflate(R.layout.list_header, null));
        stickyList.setEmptyView(findViewById(R.id.empty));
        stickyList.setDrawingListUnderStickyHeader(true);
        stickyList.setAreHeadersSticky(true);
        
        stickyList.setAdapter(mAdapter);
		
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        
        stickyList.setStickyHeaderTopOffset(0);

	}
	@Override
	public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
		/*try {
			JSONArray  ForUrl = null;
			JSONObject ForUrl2= null;
			ForUrl = new JSONArray(pager);
			ForUrl2= new JSONObject(ForUrl.getString(position));
			String sub = ForUrl2.getString("sub");
			ForUrl = new JSONArray(sub);
			ForUrl2 = new JSONObject(ForUrl.getString(0));
			
			String url = ForUrl2.getString("url");
			Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
			startActivity(intent);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getBaseContext(), e.toString(), 5000).show();
		}*/
		   Toast.makeText(this, "Item " + position + " clicked!", Toast.LENGTH_SHORT).show();
	}
	
	@Override
    public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
        Toast.makeText(this, "Header " + headerId + " currentlySticky ? " + currentlySticky, Toast.LENGTH_SHORT).show();
    }
	
	@Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {
        if (fadeHeader && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            header.setAlpha(1 - (offset / (float) header.getMeasuredHeight()));
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setAlpha(1);
    }
    
	private ArrayList<JsonNewsItemForEvent> SearchQueryMaker(String query){

		ArrayList<JsonNewsItemForEvent> jsonfile = new ArrayList<JsonNewsItemForEvent>();
		ArrayList<JsonNewsItemForEvent> orderedjson = new ArrayList<JsonNewsItemForEvent>();

		JSONObject test = null;
	    JSONObject corp;
        ArrayList<String> corp_name;
        ArrayList<String> corp_url;
	    String sample = null;
        String line;
        String title = "a"; 
	    String page ="";
	    String image_url="";
	    Bitmap bmImg =null;
	    int date = 0;
        int count =0;
		String group1 = "http://json.core.today/json2/?opt=2&n=20&o=1&q=";
		Log.e("DUNKIN@@@", query);
		try{
			sample = group1 + URLEncoder.encode(query, "UTF-8");
		}
		catch(UnsupportedEncodingException e1){
			Toast.makeText(getApplication(), e1.toString() + "---THE PROBLEM IN URL ADRRESS", Toast.LENGTH_LONG).show();
		}
	    	JSONArray alljson;
	        URL url;
	        HttpURLConnection urlConnection;
	        BufferedReader bufreader;
	        try {
			url = new URL(sample);		
			urlConnection = (HttpURLConnection) url.openConnection();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			while((line=bufreader.readLine())!=null){
				page+=line;
			}
			
			alljson = new JSONArray(page);

	        for(int k =0; k < 20 ; count++){
	         	  
	                 test=new JSONObject(alljson.getString(count));
	                
	                 JSONArray corpInfo = new JSONArray(test.getString("a"));
	                 
	                 	try{
	                 	  title = test.getString("ti");
	                 	  image_url = test.getString("i");
	                 	  date = test.getInt("day");
	                 	  corp_name = new ArrayList<String>();
	                 	  corp_url = new ArrayList<String>();
	                 	  for(int i = 0; i <corpInfo.length(); i++){
	 	                	  corp = new JSONObject(corpInfo.getString(i));
	 	                	  
	 	                	  corp_name.add(corp.getString("corp"));
	 	                	  corp_url.add(corp.getString("url"));
	 	                	 // Log.e("DUNKIN", corp_name.get(i) + "  ");
	 	                	 
	                 	  }
	                 	//  Log.e("TITLE", title);
	                       jsonfile.add(new JsonNewsItemForEvent(title, corp_name, corp_url,image_url,bmImg,date));
	                       //publishProgress(jsonfile.get(k));
	                       //SystemClock.sleep(150);
	                       Log.e("DUNKIN",jsonfile.get(k).getTitle());
	                       Log.e("DUNKIN@","Date is "+jsonfile.get(k).getDate());
	                       k++;
	                 	}
	                     catch(Exception e){
	                     	Log.e("ERROR","Problem is  "+e);
	                     }
	            }
			urlConnection.disconnect();
			
			}
			catch(Exception e){

			}

	        //ordering jsonfile by time order, higher to 
	        return jsonfile;
	    }
	/*
	private String CategoryMaker(){

	    String line;
	    String page ="";
		String group1 = "http://kr.core.today/json2/?n=10&sl=1&s=-1&c="+category;
	    String sample = group1; 
	    Toast.makeText(getBaseContext(), sample, 5000).show();
	        URL url;
	        HttpURLConnection urlConnection;
	        BufferedReader bufreader;
	        try {
			url = new URL(sample);		

			urlConnection = (HttpURLConnection) url.openConnection();
			Toast.makeText(getBaseContext(), "111111111111111", 10000).show();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			Toast.makeText(getBaseContext(), "222222222222222", 10000).show();
			while((line=bufreader.readLine())!=null){
				page+=line;
			}
			Toast.makeText(getBaseContext(), page, 6000).show();
			urlConnection.disconnect();
			
			}
			catch(Exception e){
				Toast.makeText(getBaseContext(), e.toString(), 3000).show();
			}

	        return page;
	    }
	    */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	

}
